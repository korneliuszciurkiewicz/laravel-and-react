import React from 'react';
import './Cards.css';
import CardItem from './CardItem';

function Cards() {
  return (
    <div className='cards'>
      <h1>Odnajdź siebie w podróży przez najpiękniejsze miejsca świata</h1>
      <div className='cards__container'>
        <div className='cards__wrapper'>
          <ul className='cards__items'>
            <CardItem
              src='images/img-9.jpg'
              text='Odkrywaj niezwykłe widoki w Dżungli Amazońskiej'
              label='Przygoda'
              path='/services'
            />
            <CardItem
              src='images/img-2.jpg'
              text='Zwiedzaj Wyspy Bali na prywatnym jachcie'
              label='Luksus'
              path='/services'
            />
          </ul>
          <ul className='cards__items'>
            <CardItem
              src='images/img-3.jpg'
              text='Wybierz się na rejs po nieskończonych wodach Atlantyku'
              label='Mistyka'
              path='/services'
            />
            <CardItem
              src='images/img-4.jpg'
              text='Zagraj w piłkę na szczytach Dachu Świata'
              label='Przygoda'
              path='/products'
            />

</ul>

<ul className='cards__items'>
            <CardItem
              src='images/img-10.jpg'
              text='Poznaj serce dzikiej przyrody'
              label='Natura'
              path='/sign-up'
            />

<CardItem
              src='images/img-11.jpg'
              text='Zobacz na własne oczy historię'
              label='Zabytki'
              path='/sign-up'
            />

</ul>


<ul className='cards__items'>
<CardItem
              src='images/img-12.jpg'
              text='Odkryj duchową tradycję świata'
              label='Mistycyzm'
              path='/sign-up'
            />






<CardItem
              src='images/img-8.jpg'
              text='Wyrusz w podróż przez Saharę na wielbłądzie'
              label='Adrenalina'
              path='/sign-up'
            />


</ul>





         
        </div>
      </div>
    </div>
  );
}

export default Cards;
