import React from 'react';
import './Footer.css';
import { Button } from './Button';
import { Link } from 'react-router-dom';

function Footer() {
  return (
    <div className='footer-container'>
      <section className='footer-subscription'>
        <p className='footer-subscription-heading'>
         Zapisz się do Podrózniczego Newslettera, aby mieć świeże informacje.
        </p>
        <p className='footer-subscription-text'>
          W każdym momencie możesz cofnąć subskrypcję.
        </p>
        <div className='input-areas'>
          <form>
            <input
              className='footer-input'
              name='email'
              type='email'
              placeholder='Your Email'
            />
            <Button buttonStyle='btn--outline'>Zapisz do newslettera</Button>
          </form>
        </div>
      </section>
      <div class='footer-links'>
        <div className='footer-link-wrapper'>
          <div class='footer-link-items'>
            <h2>O nas</h2>
            <Link to='/sign-up'>Jak działamy?</Link>
            <Link to='/referentions'>Referencje</Link>
            <Link to='/join-us'>Wspólna kariera</Link>
            <Link to='/invest'>Inwestorzy</Link>
            <Link to='/conditions'>Warunki usługi</Link>
          </div>
          <div class='footer-link-items'>
            <h2>Skontaktuj się z nami</h2>
            <Link to='/contact'>Kontakt</Link>
            <Link to='/support'>Wsparcie</Link>
            <Link to='/map'>Cele podrózy </Link>
            <Link to='/sponsor'>Sponsorowanie</Link>
          </div>
        </div>
        <div className='footer-link-wrapper'>
          <div class='footer-link-items'>
            <h2>Filmy</h2>
            <Link to='/upload'>Prześlij wideo </Link>
            <Link to='/ambasadors'>Ambasadorzy</Link>
            <Link to='/travel-agencies'>Agencje podróży</Link>
            <Link to='/influencers'>Influenserzy</Link>
          </div>
          <div class='footer-link-items'>
            <h2>Media społecznościowe</h2>   
            <Link to='https://www.instagram.com/'>Instagram</Link>
            <Link to='https://www.facebook.com/'>Facebook</Link>
            <Link to='https://www.youtube.com/'>Youtube</Link>
            <Link to='https://twitter.com/'>Twitter</Link>
          </div>
        </div>
      </div>
      <section class='social-media'>
        <div class='social-media-wrap'>
          <div class='footer-logo'>
            <Link to='/' className='social-logo'>
              TRVL
              <i class='fab fa-typo3' />
            </Link>
          </div>
          <small class='website-rights'>MythicTravell © 2020</small>
          <div class='social-icons'>
            <Link
              class='social-icon-link facebook'
              to='/'
              target='_blank'
              aria-label='Facebook'
            >
              <i class='fab fa-facebook-f' />
            </Link>
            <Link
              class='social-icon-link instagram'
              to='/'
              target='_blank'
              aria-label='Instagram'
            >
              <i class='fab fa-instagram' />
            </Link>
            <Link
              class='social-icon-link youtube'
              to='/'
              target='_blank'
              aria-label='Youtube'
            >
              <i class='fab fa-youtube' />
            </Link>
            <Link
              class='social-icon-link twitter'
              to='/'
              target='_blank'
              aria-label='Twitter'
            >
              <i class='fab fa-twitter' />
            </Link>
            <Link
              class='social-icon-link twitter'
              to='/'
              target='_blank'
              aria-label='LinkedIn'
            >
              <i class='fab fa-linkedin' />
            </Link>
          </div>
        </div>
      </section>
    </div>
  );
}

export default Footer;
